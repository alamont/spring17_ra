# -*- coding: UTF-8 -*-

import itertools

class Cand:
	def __init__(self, form, violations):
		self.form = form
		self.vios = violations

	def harmony(self, ranking):
		score = 0
		for i in range(len(ranking)):
			score += self.vios[i] * ranking[i]
		return score

cons = ['*si', '*VV', 'Ident', 'Max']

print 'No Serial Markedness'
print '--------------------'
print '\tsu\tsu-a\tsu-e\tsi\tsi-a\tsi-e\ttas\ttas-a\ttas-e\ttapo\ttapo-a\ttapo-e'

step_one = {
# /su/
'su' : [Cand('su', [0,0,0,0]), 
        Cand('s',  [0,0,0,1]), 
        Cand('ʃu', [0,0,1,0])],
# /su-a/
'su-a' : [Cand('sua', [0,1,0,0]),
          Cand('sa',  [0,0,0,1]),
          Cand('ʃua', [0,1,1,0])],
# /su-e/
'su-e' : [Cand('sue', [0,1,0,0]),
          Cand('se',  [1,0,0,1]), # Step 2
          Cand('ʃue', [0,1,1,0])],
# /si/
'si' : [Cand('si', [1,0,0,0]),
        Cand('s',  [0,0,0,1]),
        Cand('ʃi', [0,0,1,0])],
# /si-a/
'si-a' : [Cand('sia', [1,1,0,0]),
          Cand('sa',  [0,0,0,1]),
          Cand('ʃia', [0,1,1,0])], # Step 2
# /si-e/
'si-e' : [Cand('sie', [1,1,0,0]),
          Cand('se',  [1,0,0,1]), # Step 2
          Cand('ʃie', [0,1,1,0])], # Step 2
# /tas/
'tas' : [Cand('tas', [0,0,0,0]),
        Cand('ts',  [0,0,0,1]),
        Cand('taʃ', [0,0,1,0])],
# /tas-a/
'tas-a' : [Cand('tasa', [0,0,0,0]),
          Cand('tas',  [0,0,0,1]),
          Cand('taʃa', [0,0,1,0])],
# /tas-e/
'tas-e' : [Cand('tase', [1,0,0,0]),
          Cand('tas',  [0,0,0,1]),
          Cand('taʃe', [0,0,1,0])],
# /tapo/
'tapo' : [Cand('tapo', [0,0,0,0]),
        Cand('tap',  [0,0,0,1]),
        Cand('tapjo', [0,0,1,0])],
# /tapo-a/
'tapo-a' : [Cand('tapoa', [0,1,0,0]),
          Cand('tapa',  [0,0,0,1]),
          Cand('tapjoa', [0,1,1,0])],
# /tapo-e/
'tapo-e' : [Cand('tapoe', [0,1,0,0]),
          Cand('tape',  [1,0,0,1]),
          Cand('tapjoe', [0,1,1,0])]
}

step_two = {
# /su-e/ > se
'su-e>se' : [Cand('se', [1,0,0,0]),
            Cand('s',  [0,0,0,1]),
            Cand('ʃe', [0,0,1,0])],

# /si-a/ > ʃia
'si-a>ʃia' : [Cand('ʃia', [0,1,0,0]),
             Cand('ʃa',  [0,0,0,1])],

# /si-e/ > se
'si-e>se' : [Cand('se', [1,0,0,0]),
            Cand('s',  [0,0,0,1]),
            Cand('ʃe', [0,0,1,0])],

# /si-e/ > ʃie
'si-e>ʃie' : [Cand('ʃie', [0,1,0,0]),
             Cand('ʃe',  [0,0,0,1])]
}

def winner(input, ranking):
	minimum_harmony = 10000000000
	output = ''
	for cand in step_one[input]:
		if cand.harmony(ranking) < minimum_harmony:
			minimum_harmony = cand.harmony(ranking)
			output = cand.form
	inout = input + '>' + output
	if inout in step_two:
		minimum_harmony = 10000000000
		for cand in step_two[inout]:
			if cand.harmony(ranking) < minimum_harmony:
				minimum_harmony = cand.harmony(ranking)
				output = cand.form
	return output

typ = itertools.permutations(cons)
inputs = ['su', 'su-a', 'su-e', 'si', 'si-a', 'si-e', 'tas', 'tas-a', 'tas-e', 'tapo', 'tapo-a', 'tapo-e']

langrank = {}

for gram in typ:
	si = 6-gram.index('*si')
	vv = 6-gram.index('*VV')
	id = 6-gram.index('Ident')
	ma = 6-gram.index('Max')
	ranking = [10**si, 10**vv, 10**id, 10**ma]
	lang = ''
	for input in inputs:
		lang += '\t' + winner(input, ranking)
	if lang in langrank:
		langrank[lang].append(gram)
	else:
		langrank[lang] = [gram]

sortedlangs = sorted(langrank.keys())

for i in range(len(sortedlangs)):
	print i, sortedlangs[i]

for i in range(len(sortedlangs)):
	print
	print 'Rankings that produce language', i, '(' + str(len(langrank[sortedlangs[i]])) + ')'
	for rank in langrank[sortedlangs[i]]:
		print '>>'.join(rank)
