# -*- coding: UTF-8 -*-

import itertools

class Cand:
	def __init__(self, form, violations):
		self.form = form
		self.vios = violations

	def harmony(self, ranking):
		score = 0
		for i in range(len(ranking)):
			score += self.vios[i] * ranking[i]
		return score

cons = ['*si', '*VV', 'Ident', 'Max', 'SM(*si,*VV)','SM(*VV,*si)']

print 'Yes Serial Markedness'
print '--------------------'
print '\tsu\tsu-a\tsu-e\tsi\tsi-a\tsi-e\ttas\ttas-a\ttas-e\ttapo\ttapo-a\ttapo-e'

step_one = {
# si vv ident max si-vv vv-si
# /su/
'su' : [Cand('su', [0,0,0,0,0,0]), 
        Cand('s',  [0,0,0,1,0,0]), 
        Cand('ʃu', [0,0,1,0,0,0])],
# /su-a/
'su-a' : [Cand('sua',    [0,1,0,0,0,0]),
          Cand('sa<vv>', [0,0,0,1,0,0]),
          Cand('ʃua',    [0,1,1,0,0,0])],
# /su-e/
'su-e' : [Cand('sue',    [0,1,0,0,0,0]),
          Cand('se<vv>', [1,0,0,1,0,0]), # Step 2
          Cand('ʃue',    [0,1,1,0,0,0])],
# /si/
'si' : [Cand('si',     [1,0,0,0,0,0]),
        Cand('s<si>',  [0,0,0,1,0,0]),
        Cand('ʃi<si>', [0,0,1,0,0,0])],
# /si-a/
'si-a' : [Cand('sia',        [1,1,0,0,0,0]),
          Cand('sa<vv&si>',  [0,0,0,1,1,1]),
          Cand('ʃia<si>',    [0,1,1,0,0,0])], # Step 2
# /si-e/
'si-e' : [Cand('sie',        [1,1,0,0,0,0]),
          Cand('se<vv&si>',  [1,0,0,1,1,1]), # Step 2
          Cand('ʃie<si>',    [0,1,1,0,0,0])], # Step 2
# /tas/
'tas' : [Cand('tas',     [0,0,0,0,0,0]),
        Cand('ts',  [0,0,0,1,0,0]),
        Cand('taʃ', [0,0,1,0,0,0])],
# /tas-a/
'tas-a' : [Cand('tasa',   [0,0,0,0,0,0]),
          Cand('tas',     [0,0,0,1,0,0]),
          Cand('taʃa',    [0,0,1,0,0,0])],
# /tas-e/
'tas-e' : [Cand('tase',   [1,0,0,0,0,0]),
          Cand('tas',     [0,0,0,1,0,0]),
          Cand('taʃe',    [0,0,1,0,0,0])],
# /tapo/ si vv id ma si-vv vv-si
'tapo' : [Cand('tapo',     [0,0,0,0,0,0]),
        Cand('tap',  [0,0,0,1,0,0]),
        Cand('tapjo', [0,0,1,0,0,0])],
# /tapo-a/
'tapo-a' : [Cand('tapoa', [0,1,0,0,0,0]),
          Cand('tapa',    [0,0,0,1,0,0]),
          Cand('tapjoa',  [0,1,1,0,0,0])],
# /tapo-e/
'tapo-e' : [Cand('tapoe', [0,1,0,0,0,0]),
          Cand('tape',    [0,0,0,1,0,0]),
          Cand('tapjoe',  [0,1,1,0,0,0])]
}

step_two = {
# si vv ident max si-vv vv-si
# /su-e/ > se<vv>
'su-e>se<vv>' : [Cand('se<vv>',    [1,0,0,0,0,0]),
                 Cand('s<vv,si>',  [0,0,0,1,1,0]),
                 Cand('ʃe<vv,si>', [0,0,1,0,1,0])],
# /si-a/ > Sia<si>
'si-a>ʃia<si>' : [Cand('ʃia<si>',    [0,1,0,0,0,0]),
          Cand('ʃa<si,vv>',  [0,0,0,1,0,1])],
# /si-e/ > se<vv>
'si-e>se<vv&si>' : [Cand('se<vv&si>',    [1,0,0,0,1,1]),
                    Cand('s<vv&si,si>',  [0,0,0,1,2,1]),
                    Cand('ʃe<vv&si,si>', [0,0,1,0,2,1])],
# /si-e/ > ʃie<si>
'si-e>ʃie<si>' : [Cand('ʃie<si>',   [0,1,0,0,0,0]),
          Cand('ʃe<si,vv>', [0,0,0,1,0,1])]
}

def winner(input, ranking):
	minimum_harmony = 10000000000
	output = ''
	for cand in step_one[input]:
		if cand.harmony(ranking) < minimum_harmony:
			minimum_harmony = cand.harmony(ranking)
			output = cand.form
	inout = input + '>' + output
	if inout in step_two:
		minimum_harmony = 10000000000
		for cand in step_two[inout]:
			if cand.harmony(ranking) < minimum_harmony:
				minimum_harmony = cand.harmony(ranking)
				output = cand.form
	return output

typ = itertools.permutations(cons)
inputs = ['su', 'su-a', 'su-e', 'si', 'si-a', 'si-e', 'tas', 'tas-a', 'tas-e', 'tapo', 'tapo-a', 'tapo-e']

langrank = {}

for gram in typ:
	si = 6-gram.index('*si')
	vv = 6-gram.index('*VV')
	id = 6-gram.index('Ident')
	ma = 6-gram.index('Max')
	sv = 6-gram.index('SM(*si,*VV)')
	vs = 6-gram.index('SM(*VV,*si)')
	ranking = [10**si, 10**vv, 10**id, 10**ma, 10**sv, 10**vs]
	lang = ''
	for input in inputs:
		lang += '\t' + winner(input, ranking)
	if lang in langrank:
		langrank[lang].append(gram)
	else:
		langrank[lang] = [gram]

sortedlangs = sorted(langrank.keys())

for i in range(len(sortedlangs)):
	print i, sortedlangs[i].replace('<vv>','').replace('<si>','').replace('<vv&si>','').replace('<vv,si>','').replace('<si,vv>','').replace('<vv&si,si>','')

for i in range(len(sortedlangs)):
	print
	print 'Rankings that produce language', i, '(' + str(len(langrank[sortedlangs[i]])) + ')'
	for rank in langrank[sortedlangs[i]]:
		print '>>'.join(rank)
