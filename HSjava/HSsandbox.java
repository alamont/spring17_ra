import java.util.*;
import java.io.*;

public class HSsandbox {

	public static void main(String[] args) {

		// For the sake of sandboxing, these are read from the command line:

		//si-vv
		// Order of constraint args: Ident 	Max 	*VV 	*si 	SM(*si,*VV) 	SM(*VV,*si)

		//paka
		// Order of constraint args: IdentStress	IdentLength	MainLeft	MainRight	*V:	WSP

		// List of operations
		ArrayList<String[]> operations = new ArrayList<String[]>();

		// Read the operations from a file
//		String opsFile = "Operations-paka.txt";
		String opsFile = "Operations-sivv.txt";
		String line, splitOne, splitTwo;
		int split;
		try {
			FileReader fileReader = new FileReader(opsFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				String[] tempOp = new String[2];
				split = line.indexOf("\t");
				splitOne = line.substring(0, split);
				splitTwo = line.substring(split + 1);
				tempOp[0] = splitOne;
				tempOp[1] = splitTwo;
				operations.add(tempOp);
			}
			bufferedReader.close();
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file");
		}
		catch (IOException ex) {
			System.out.println("Error reading file");
		}

		// Constraints
		ArrayList<String> allConstraints = new ArrayList<String>();
		ArrayList<String[]> faithfulnessConstraints = new ArrayList<String[]>();
		ArrayList<String[]> markednessConstraints = new ArrayList<String[]>();
		ArrayList<String[]> serialMarkednessConstraints = new ArrayList<String[]>();
		ArrayList<String> markednessForSMConstraints = new ArrayList<String>();
		String[] tempConTwo = new String[2];
		String[] tempConThree = new String[3];

		// Read the constraints from a file
//		String consFile = "Constraints-paka.txt";
		String consFile = "Constraints-sivv.txt";
		line = null;
		int splSplit = -1;
		int lengthConstraintNames = 0;
		String splitThree = "";

		int state = 0;
		try {
			FileReader fileReader = new FileReader(consFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				// Faithfulness
				if (state == 0) {
					if (line.equals("Faithfulness")) {
						state = 1;
					}
				} else if (state == 1) {
					if (line.equals("Markedness")) {
						state = 2;
					} else {
						tempConThree = new String[3];
						split = line.indexOf("\t");
						splSplit = line.indexOf("\t", split + 1);
						splitOne = line.substring(0, split);
						splitTwo = line.substring(split + 1, splSplit);
						splitThree = line.substring(splSplit + 1);
						tempConThree[0] = splitOne;
						tempConThree[1] = splitTwo;
						tempConThree[2] = splitThree;
						faithfulnessConstraints.add(tempConThree);
						allConstraints.add(splitOne);
						lengthConstraintNames += splitOne.length();
					}
				// Markedness
				} else if (state == 2) {
					if (line.equals("Serial Markedness")) {
						state = 3;
					} else {
						tempConTwo = new String[2];
						split = line.indexOf("\t");
						splitOne = line.substring(0, split);
						splitTwo = line.substring(split + 1);
						tempConTwo[0] = splitOne;
						tempConTwo[1] = splitTwo;
						markednessConstraints.add(tempConTwo);
						allConstraints.add(splitOne);
						lengthConstraintNames += splitOne.length();
					}
				// Serial Markedness
				} else if (state == 3) {
					tempConThree = new String[3];
					split = line.indexOf("\t");
					splSplit = line.indexOf("\t", split + 1);
					splitOne = line.substring(0, split);
					splitTwo = line.substring(split + 1, splSplit);
					splitThree = line.substring(splSplit + 1);
					tempConThree[0] = splitOne;
					tempConThree[1] = splitTwo;
					tempConThree[2] = splitThree;
					serialMarkednessConstraints.add(tempConThree);
					// Keep track of which markedness constraints are used by SM constraints
					if (markednessForSMConstraints.indexOf(splitTwo) == -1) {
						markednessForSMConstraints.add(splitTwo);
					}
					if (markednessForSMConstraints.indexOf(splitThree) == -1) {
						markednessForSMConstraints.add(splitThree);
					}
					allConstraints.add(splitOne);
					lengthConstraintNames += splitOne.length();
				}
			}
			bufferedReader.close();
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file");
		}
		catch (IOException ex) {
			System.out.println("Error reading file");
		}

		int numberConstraints = allConstraints.size();

		// Constraint Rank
		Map<String, Double> weights = new HashMap<String, Double>();
		int[] constraintRank = new int[numberConstraints];
		int thisInt;
		for (int i = 1; i < args.length; i++) {
			thisInt = Integer.valueOf(args[i]);
			constraintRank[i - 1] = thisInt;
		}
		weights.put(allConstraints.get(findIndex(constraintRank, 0)), 1.0);
		for (int i = 1; i < numberConstraints; i++) {
			weights.put(allConstraints.get(findIndex(constraintRank, i)), Math.pow(10, -i));
		}

		// Variable declaration before looping
		ArrayList<String[]> candidateSet;
		String[] fullyFaithful;
		Map<String[], Double> harmony;
		int theseViolations, constraintWeight, underscoreIndex, lineLength;
		double oldVios;
		String tableauInput = "";
//		String bestCand = "";
		Boolean converged;
		Boolean firstStep = true;

		ArrayList<String> convergedCandidates = new ArrayList<String>();
		ArrayList<String> notConvergedCandidates = new ArrayList<String>();
		// Add first input to notConvergedCandidates
		notConvergedCandidates.add(args[0] + "<>");

		// Loop until notConvergedCandidates is empty
		while (!notConvergedCandidates.isEmpty()) {
			// Loop until candidate converges
			do {
				// Print constraint ranking
				System.out.print(allConstraints.get(findIndex(constraintRank, 0)));
				for (int i = 1; i < numberConstraints; i++) {
					System.out.print(" >> ");
					System.out.print(allConstraints.get(findIndex(constraintRank, i)));
				}
				System.out.println();

				// First step: derivation input with empty MSEQ
	//			if (firstStep) {
	//				tableauInput = args[0] + "<>";
	//				firstStep = false;
	//			}
				// Best candidate from last step
	//			else {
	//				tableauInput = bestCand;
	//			}

				// Pop next input from notConvergedCandidates
				tableauInput = notConvergedCandidates.get(0);
				notConvergedCandidates.remove(0);

				// Generate candidates
				candidateSet = new ArrayList<String[]>();

				// Add fully faithful candidate
				if (opsFile.equals("Operations-paka.txt")) {
					// Check for stress
					boolean tableauInputHasStress = false;
					for (int i = 0; i < tableauInput.indexOf("<") + 1; i++) {
						if (tableauInput.substring(i, i+1).equals("A")) {
							tableauInputHasStress = true;
						} else if (tableauInput.substring(i, i+1).equals("E")) {
							tableauInputHasStress = true;
						}
					}
					if (tableauInputHasStress) {
						fullyFaithful = new String[2];
						fullyFaithful[0] = tableauInput;
						fullyFaithful[1] = tableauInput;
						candidateSet.add(fullyFaithful);
					}
				} else {
					fullyFaithful = new String[2];
					fullyFaithful[0] = tableauInput;
					fullyFaithful[1] = tableauInput;
					candidateSet.add(fullyFaithful);
				}

				// Add unfaithful candidates
				for (int i = 0; i < operations.size(); i++) {
					candidateSet.addAll(operation(tableauInput, operations.get(i)[0], operations.get(i)[1]));
				}

				int longestCandidateLength = 0;

				// Update candidate MSEQ's
				for (int i = 0; i < candidateSet.size(); i++) {
					candidateSet.get(i)[1] = updateMSEQ(candidateSet.get(i), markednessForSMConstraints, markednessConstraints);
					if (candidateSet.get(i)[1].length() > longestCandidateLength) {
						longestCandidateLength = candidateSet.get(i)[1].length();
					}
				}

				// Print input
				System.out.print("/");
				System.out.print(tableauInput);
				System.out.print("/");
				for (int i = 0; i < longestCandidateLength - tableauInput.length(); i++) {
					System.out.print(" ");
				}
				System.out.print("\t\t");

				// Print constraints (not in ranked order)
				for (int i = 0; i < allConstraints.size(); i++) {
					System.out.print(allConstraints.get(i));
					System.out.print("\t");
				}
				System.out.println("|\tHarmony");

				// Pretty printing line below input and constraints
				lineLength = (5 * allConstraints.size()) + lengthConstraintNames + longestCandidateLength + 20;
				for (int ll = 0; ll < lineLength; ll++) {
					System.out.print("-");
				}
				System.out.println();

				// Initialize harmony scores to 0
				harmony = new HashMap<String[], Double>();
				for (int i = 0; i < candidateSet.size(); i++) {
					harmony.put(candidateSet.get(i), 0.0);
				}

				// To keep track of the best candidate
				double minimalHarmony = 999999999.9;
//				bestCand = "";

				// Loop over candidates
				for (int i = 0; i < candidateSet.size(); i++) {
					// Print candidate
					System.out.print(candidateSet.get(i)[1]);
					for (int j = 0; j < longestCandidateLength - candidateSet.get(i)[1].length(); j++) {
						System.out.print(" ");
					}
					System.out.print("\t\t\t");
					oldVios = 0.0;

					// Faithfulness violations
					for (int j = 0; j < faithfulnessConstraints.size(); j++) {
						theseViolations = getViolations(candidateSet.get(i), "faith", faithfulnessConstraints.get(j));
						System.out.print(theseViolations);
						System.out.print("\t\t");
						oldVios += theseViolations * weights.get(faithfulnessConstraints.get(j)[0]);
					harmony.put(candidateSet.get(i), oldVios);
					}

					// Build array for Serial Markedness violations
					// Diagonal = number of times a markedness constraint was seen ; [x][y] = x preceded y
					int[][] orderedMark = new int[markednessForSMConstraints.size()][markednessForSMConstraints.size()];

					String candidateMSEQ = candidateSet.get(i)[1].substring(candidateSet.get(i)[1].indexOf("<") + 1, candidateSet.get(i)[1].length() - 1);
					if (candidateMSEQ.length() > 0) {
						String[] splitMSEQ = candidateMSEQ.split(";");
						for (int j = 0; j < splitMSEQ.length; j++) {
							String[] splitSplit = splitMSEQ[j].split("&");
							for (int k = 0; k < splitSplit.length; k++) {
								int consIndexOne = markednessForSMConstraints.indexOf(splitSplit[k]);
								// Increase number of times seen constraint
								orderedMark[consIndexOne][consIndexOne] += 1;
								// Increase sequence counts
								for (int l = k + 1; l < splitSplit.length; l++) {
									int consIndexEll = markednessForSMConstraints.indexOf(splitSplit[l]);
									orderedMark[consIndexOne][consIndexEll] += orderedMark[consIndexEll][consIndexEll];
								}
								if (splitSplit.length > 1) {
									// Increase sequence counts from conjunction
									for (int m = k + 1; m < splitSplit.length; m++) {
										int consIndexTwo = markednessForSMConstraints.indexOf(splitSplit[m]);
										orderedMark[consIndexOne][consIndexTwo] += 1;
										orderedMark[consIndexTwo][consIndexOne] += 1;
									}
								}
							}
						}
					}

					// Remove "_" from candidates
					underscoreIndex = candidateSet.get(i)[0].indexOf("_");
					if (underscoreIndex > -1) {
						candidateSet.get(i)[0] = candidateSet.get(i)[0].substring(0, underscoreIndex) + candidateSet.get(i)[0].substring(underscoreIndex + 1);
					}
					underscoreIndex = candidateSet.get(i)[1].indexOf("_");
					if (underscoreIndex > -1) {
						candidateSet.get(i)[1] = candidateSet.get(i)[1].substring(0, underscoreIndex) + candidateSet.get(i)[1].substring(underscoreIndex + 1);
					}

					// Markedness violations
					oldVios = harmony.get(candidateSet.get(i));
					for (int j = 0; j < markednessConstraints.size(); j++) {
						theseViolations = getViolations(candidateSet.get(i), "mark", markednessConstraints.get(j));
						System.out.print(theseViolations);
						System.out.print("\t\t");
						oldVios += theseViolations * weights.get(markednessConstraints.get(j)[0]);
					}

					// Serial Markedness violations
					for (int j = 0; j < serialMarkednessConstraints.size(); j++) {
						int x = markednessForSMConstraints.indexOf(serialMarkednessConstraints.get(j)[1]);
						int y = markednessForSMConstraints.indexOf(serialMarkednessConstraints.get(j)[2]);
						theseViolations = orderedMark[x][y];
						System.out.print(theseViolations);
						System.out.print("\t\t");
						oldVios += theseViolations * weights.get(serialMarkednessConstraints.get(j)[0]);
					}
					harmony.put(candidateSet.get(i), oldVios);

					// Print candidate harmony
					System.out.print("|\t");
					System.out.println(harmony.get(candidateSet.get(i)));
					// Update best candidate
					if (harmony.get(candidateSet.get(i)) < minimalHarmony) {
						minimalHarmony = harmony.get(candidateSet.get(i));
//						bestCand = candidateSet.get(i)[1];
					}
				}

				// Pretty printing bottom of tableau
				for (int ll = 0; ll < lineLength; ll++) {
					System.out.print("-");
				}
				System.out.println("\n");

				System.out.println("Optimal candidates:");
//				System.out.println(bestCand);

				converged = false;
				// Find optimal candidates
				for (int i = 0; i < candidateSet.size(); i++) {
					if (harmony.get(candidateSet.get(i)) == minimalHarmony) {
						System.out.print(candidateSet.get(i)[1]);
						System.out.print("\t");
						if (candidateSet.get(i)[1].equals(tableauInput)) {
							converged = true;
							convergedCandidates.add(candidateSet.get(i)[1]);
							System.out.println("Converged!\n");
						} else {
							notConvergedCandidates.add(candidateSet.get(i)[1]);
							System.out.println("Not converged!\n");
						}
					}
				}
			} while (!converged);
		}
		System.out.println("Final outputs:");
		for (int i = 0; i < convergedCandidates.size(); i++) {
			if (convergedCandidates.indexOf(convergedCandidates.get(i)) == i) {
				System.out.println(convergedCandidates.get(i));
			}
		}
	}

	public static int getViolations(String[] inputCandidate, String type, String[] mark) {
		int numberViolations = 0;
		if (type.equals("faith")) {
			// Remove MSEQ from input and output
			String faithInput = inputCandidate[0].substring(0, inputCandidate[0].indexOf("<"));
			String faithOutput = inputCandidate[1].substring(0, inputCandidate[1].indexOf("<"));

			// ugly implementation for Max because right there aren't features
			if (mark[0].equals("Max")) {
				for (int i = 0; i < faithOutput.length(); i++) {
					if (faithOutput.substring(i, i+1).equals("_")) {
						numberViolations++;
					}
				}
			} else if (mark[0].equals("IdentStress")) {
				for (int i = 0; i < faithOutput.length(); i++) {
					if (faithOutput.substring(i, i+1).equals("a")) {
						if (faithInput.substring(i, i+1).equals("A")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("A")) {
						if (faithInput.substring(i, i+1).equals("a")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("e")) {
						if (faithInput.substring(i, i+1).equals("E")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("E")) {
						if (faithInput.substring(i, i+1).equals("e")) {
							numberViolations++;
						}
					}
				}
			} else if (mark[0].equals("IdentLength")) {
				for (int i = 0; i < faithOutput.length(); i++) {
					if (faithOutput.substring(i, i+1).equals("a")) {
						if (faithInput.substring(i, i+1).equals("e")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("A")) {
						if (faithInput.substring(i, i+1).equals("E")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("e")) {
						if (faithInput.substring(i, i+1).equals("a")) {
							numberViolations++;
						}
					} else if (faithOutput.substring(i, i+1).equals("E")) {
						if (faithInput.substring(i, i+1).equals("A")) {
							numberViolations++;
						}
					}
				}
			} else {
				// Regular definition
				for (int i = 0; i < faithInput.length(); i++) {
					if (faithInput.substring(i, i+1).equals(mark[1])) {
						if (faithOutput.substring(i, i+1).equals(mark[2])) {
							numberViolations++;
						}
					}
				}
			}
		} else if (type.equals("mark")) {
			// Remove MSEQ from output
			String markOutput = inputCandidate[1].substring(0, inputCandidate[1].indexOf("<"));

			// ugly for *VV because there aren't features
			if (mark[0].equals("*VV")) {
				String[] vowelpairs = new String[16];
				vowelpairs[0] = "aa";
				vowelpairs[1] = "ae";
				vowelpairs[2] = "ai";
				vowelpairs[3] = "au";
				vowelpairs[4] = "ea";
				vowelpairs[5] = "ee";
				vowelpairs[6] = "ei";
				vowelpairs[7] = "eu";
				vowelpairs[8] = "ia";
				vowelpairs[9] = "ie";
				vowelpairs[10] = "ii";
				vowelpairs[11] = "iu";
				vowelpairs[12] = "ua";
				vowelpairs[13] = "ue";
				vowelpairs[14] = "ui";
				vowelpairs[15] = "uu";
				for (int vp = 0; vp < vowelpairs.length; vp++) {
					int lastIndex = 0;
					while (lastIndex != -1) {
						lastIndex = markOutput.indexOf(vowelpairs[vp], lastIndex);
						if (lastIndex != -1) {
							numberViolations++;
							lastIndex += 2;
						}
					}
				}
			// ugly for *si because there aren't features
			} else if (mark[0].equals("*si")) {
				String[] sigrams = new String[2];
				sigrams[0] = "si";
				sigrams[1] = "se";
				for (int sg = 0; sg < sigrams.length; sg++) {
					int lastIndex = 0;
					while (lastIndex != -1) {
						lastIndex = markOutput.indexOf(sigrams[sg], lastIndex);
						if (lastIndex != -1) {
							numberViolations++;
							lastIndex += 2;
						}
					}
				}
			} else if (mark[0].equals("MainLeft")) {
				// First vowel not A or E
				if (markOutput.substring(1, 2).equals("a")) {
					numberViolations++;
				} else if (markOutput.substring(1, 2).equals("e")) {
					numberViolations++;
				}
				// *A, *E after first syllable
//				for (int i = 2; i < markOutput.length(); i++) {
//					if (markOutput.substring(i, i+1).equals("A")) {
//						numberViolations++;
//					} else if (markOutput.substring(i, i+1).equals("E")) {
//						numberViolations++;
//					}
//				}
			} else if (mark[0].equals("MainRight")) {
				// Last vowel not A or E
				if (markOutput.substring(markOutput.length()-1).equals("a")) {
					numberViolations++;
				} else if (markOutput.substring(markOutput.length()-1).equals("e")) {
					numberViolations++;
				}
				// *A, *E before last syllable
//				for (int i = markOutput.length() - 2; i > -1; i--) {
//					if (markOutput.substring(i, i+1).equals("A")) {
//						numberViolations++;
//					} else if (markOutput.substring(i, i+1).equals("E")) {
//						numberViolations++;
//					}
//				}
			} else if (mark[0].equals("*V:")) {
				// *e, *E
				for (int i = 0; i < markOutput.length(); i++) {
					if (markOutput.substring(i, i+1).equals("e")) {
						numberViolations++;
					} else if (markOutput.substring(i, i+1).equals("E")) {
						numberViolations++;
					}
				}
			} else if (mark[0].equals("WSP")) {
				// *e
				for (int i = 0; i < markOutput.length(); i++) {
					if (markOutput.substring(i, i+1).equals("e")) {
						numberViolations++;
					}
				}
			// Here's the general case
			} else {
				int lastIndex = 0;
				while (lastIndex != -1) {
					lastIndex = markOutput.indexOf(mark[1], lastIndex);
					if (lastIndex != -1) {
						numberViolations++;
						lastIndex += mark[1].length();
					}
				}
			}
		}
		return numberViolations;
	}

	public static int findIndex(int[] haystack, int needle) {
		int location = -1;
		for (int i = 0; i < haystack.length; i++) {
			if (haystack[i] == needle) {
				location = i;
			}
		}
		return location;
	}

	public static String updateMSEQ(String[] inputCandidate, ArrayList<String> markednessForSMConstraints, ArrayList<String[]> markednessConstraints) {
		String input = inputCandidate[0];
		String candidate = inputCandidate[1];
		ArrayList<String> satisfied = new ArrayList<String>();
		for (int j = 0; j < markednessForSMConstraints.size(); j++) {
			String thisConstraint = markednessForSMConstraints.get(j);
			if (thisConstraint.equals("*VV")) {
				String[] vowelpairs = new String[16];
				vowelpairs[0] = "aa";
				vowelpairs[1] = "ae";
				vowelpairs[2] = "ai";
				vowelpairs[3] = "au";
				vowelpairs[4] = "ea";
				vowelpairs[5] = "ee";
				vowelpairs[6] = "ei";
				vowelpairs[7] = "eu";
				vowelpairs[8] = "ia";
				vowelpairs[9] = "ie";
				vowelpairs[10] = "ii";
				vowelpairs[11] = "iu";
				vowelpairs[12] = "ua";
				vowelpairs[13] = "ue";
				vowelpairs[14] = "ui";
				vowelpairs[15] = "uu";
				for (int vp = 0; vp < vowelpairs.length; vp++) {
					int lastIndex = 0;
					while (lastIndex != -1) {
						lastIndex = input.indexOf(vowelpairs[vp], lastIndex);
						if (lastIndex != -1) {
							if (!candidate.substring(lastIndex).startsWith(vowelpairs[vp])) {
								if (satisfied.indexOf("*VV") == -1) {
									satisfied.add("*VV");
								}
							}
							lastIndex += 2;
						}
					}
				}
			// ugly for *si because there aren't features
			} else if (thisConstraint.equals("*si")) {
				String[] sigrams = new String[2];
				sigrams[0] = "si";
				sigrams[1] = "se";
				for (int sg = 0; sg < sigrams.length; sg++) {
					int lastIndex = 0;
					while (lastIndex != -1) {
						lastIndex = input.indexOf(sigrams[sg], lastIndex);
						if (lastIndex != -1) {
							if (!candidate.substring(lastIndex).startsWith(sigrams[sg])) {
								if (satisfied.indexOf("*si") == -1) {
									satisfied.add("*si");
								}
							}
							lastIndex += 2;
						}
					}
				}
			// General case
//			} else {
//			String mark = markednessConstraints.get(markednessConstraints.indexOf(thisConstraint))[1];
//			int lastIndex = 0;
//			while (lastIndex != -1) {
//				lastIndex = input.indexOf(mark, lastIndex);
//				if (lastIndex != -1) {
//					if (candidate.substring(lastIndex).startsWith(mark)) {
//						if (satisfied.indexOf(thisConstraint) != -1) {
//							satisfied.add(thisConstraint);
//						}
//					}
//				}
//			}
			}
		}

		String output = candidate.substring(0, candidate.length() - 1);
		if (satisfied.size() == 1) {
			if (!output.endsWith("<")) {
				output =  output + ";";
			}
			output = output + satisfied.get(0);
		} else if (satisfied.size() > 1) {
			if (!output.endsWith("<")) {
				output =  output + ";";
			}
			for (int i = 0; i < satisfied.size(); i++) {
				if (i > 0) {
					output = output + "&";
				}
				output = output + satisfied.get(i);
			}
		}
		return output + ">";
	}

	public static ArrayList<String[]> operation(String input, String locusIn, String locusOut) {
		// FtSoS,  operations are defined as string rewrites

		ArrayList<String[]> outputCandidates = new ArrayList<String[]>();

		// Add candidates to outputCandidates
		String[] inputCandidate = new String[2];

		if (locusIn.equals("_")) {
			// Epenthesis
			for (int i = 0; i < input.indexOf("<") + 1; i++) {
				inputCandidate = new String[2];
				inputCandidate[0] = input.substring(0, i) + "_" + input.substring(i);
				inputCandidate[1] = input.substring(0, i) + locusOut + input.substring(i);
				outputCandidates.add(inputCandidate);
			}
		} else if (locusOut.equals("_")) {
			// Deletion
			// The general case is commented out below
			// FtSoS, this just deletes the first vowel
			inputCandidate = new String[2];
			String noMSEQ = input.substring(0, input.indexOf("<"));
			int firstA = noMSEQ.indexOf("a");
			int firstE = noMSEQ.indexOf("e");
			int firstI = noMSEQ.indexOf("i");
			int firstU = noMSEQ.indexOf("u");

			int firstVowel = 9999;

			if (firstA > -1) {
				firstVowel = firstA;
			}
			if (firstE > -1) {
				if (firstE < firstVowel) {
					firstVowel = firstE;
				}
			}
			if (firstI > -1) {
				if (firstI < firstVowel) {
					firstVowel = firstI;
				}
			}
			if (firstU > -1) {
				if (firstU < firstVowel) {
					firstVowel = firstU;
				}
			}

			if (firstVowel < 9999) {
				inputCandidate[0] = input;
				inputCandidate[1] = input.substring(0, firstVowel) + "_" + input.substring(firstVowel + 1);
				outputCandidates.add(inputCandidate);
			}
//			for (int i = 0; i < input.indexOf("<") + 1; i++) {
//				if (input.substring(i, i+1).equals(locusIn)) {
//					inputCandidate[0] = input;
//					inputCandidate[1] = input.substring(0, i) + "_" + input.substring(i+1);
//					outputCandidates.add(inputCandidate);
//				}
//			}
//		} else if (locusIn.equals("a")) {
			// stress is no longer an operation -> built into length changing operation
//		}
		} else if (locusIn.equals("e")) {
			// paka change length and respect cumulativity
			// Strip stress from input
			String unstressed = input;
			for (int i = 0; i < unstressed.indexOf("<") + 1; i++) {
				if (unstressed.substring(i, i+1).equals("A")) {
					unstressed = unstressed.substring(0, i) + "a" + unstressed.substring(i+1);
				} else if (unstressed.substring(i, i+1).equals("E")) {
					unstressed = unstressed.substring(0, i) + "e" + unstressed.substring(i+1);
				}
			}
			// Change length
			for (int i = 0; i < unstressed.indexOf("<") + 1; i++) {
				String lengthChanged = "";
				if (unstressed.substring(i, i+1).equals("a")) {
					lengthChanged = unstressed.substring(0, i) + "e" + unstressed.substring(i+1);
					// Assign stress
					for (int j = 0; j < lengthChanged.indexOf("<") + 1; j++) {
						inputCandidate = new String[2];
						if (lengthChanged.substring(j, j+1).equals("a")) {
							inputCandidate[0] = input;
							inputCandidate[1] = lengthChanged.substring(0, j) + "A" + lengthChanged.substring(j+1);
							outputCandidates.add(inputCandidate);
						} else if (lengthChanged.substring(j, j+1).equals("e")) {
							inputCandidate[0] = input;
							inputCandidate[1] = lengthChanged.substring(0, j) + "E" + lengthChanged.substring(j+1);
							outputCandidates.add(inputCandidate);
						}
					}
				} else if (unstressed.substring(i, i+1).equals("e")) {
					lengthChanged = unstressed.substring(0, i) + "a" + unstressed.substring(i+1);
					// Assign stress
					for (int j = 0; j < lengthChanged.indexOf("<") + 1; j++) {
						inputCandidate = new String[2];
						if (lengthChanged.substring(j, j+1).equals("a")) {
							inputCandidate[0] = input;
							inputCandidate[1] = lengthChanged.substring(0, j) + "A" + lengthChanged.substring(j+1);
							outputCandidates.add(inputCandidate);
						} else if (lengthChanged.substring(j, j+1).equals("e")) {
							inputCandidate[0] = input;
							inputCandidate[1] = lengthChanged.substring(0, j) + "E" + lengthChanged.substring(j+1);
							outputCandidates.add(inputCandidate);
						}
					}
				}
			}
		} else {
			// Substitution
			for (int i = 0; i < input.indexOf("<") + 1; i++) {
				inputCandidate = new String[2];
				if (input.substring(i, i+1).equals(locusIn)) {
					inputCandidate[0] = input;
					inputCandidate[1] = input.substring(0, i) + locusOut + input.substring(i+1);
					outputCandidates.add(inputCandidate);
				}
			}
		}
		return outputCandidates;
	}
}
