import java.util.*;
import java.io.*;

public class paradigmReader {

	public static void main(String[] args) {

		// Segment -> features map
		Map<String, double[]> segmentFeatures = new HashMap<String, double[]>();
		Map<String, String> featuresSegment = new HashMap<String, String>();
		String[] features = new String[1];
		ArrayList<String> segments = new ArrayList<String>();

		// Read the features from a file
		String featsFile = "Features-sivv.txt";
//		String featsFile = "Features-paka.txt";
		String line;
		try {
			FileReader fileReader = new FileReader(featsFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			while ((line = bufferedReader.readLine()) != null) {
				String[] lineSplit = line.split("\t");
				if (lineSplit[0].equals("FEATURES")) {
					features = new String[lineSplit.length - 1];
					features = Arrays.copyOfRange(lineSplit, 1, lineSplit.length);
				} else {
					double[] tempFeatures = new double[lineSplit.length - 1];
					for (int i = 1; i < lineSplit.length; i++) {
						tempFeatures[i-1] = Double.parseDouble(lineSplit[i]);
					}
					segmentFeatures.put(lineSplit[0], tempFeatures);
					String featSegKey = "";
					for (int i = 0; i < tempFeatures.length; i++) {
						featSegKey = featSegKey + Double.toString(tempFeatures[i]) + ",";
					}
					featuresSegment.put(featSegKey, lineSplit[0]);
					segments.add(lineSplit[0]);
				}
			}
			bufferedReader.close();
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file");
		}
		catch (IOException ex) {
			System.out.println("Error reading file");
		}
		// Read the data from a file
		String paradigmFile = "Paradigm-sivv.txt";
//		String paradigmFile = "Paradigm-paka.txt";
		ArrayList<String[]> paradigm = new ArrayList<String[]>();
		try {
			FileReader fileReader = new FileReader(paradigmFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			line = null;
			int numColumns = 0;
			while ((line = bufferedReader.readLine()) != null) {
				paradigm.add(line.split("\t"));
				System.out.println(line);
			}
			bufferedReader.close();
		}
		catch (FileNotFoundException ex) {
			System.out.println("Unable to open file");
		}
		catch (IOException ex) {
			System.out.println("Error reading file");
		}

		// SANITY
		System.out.println("\nFEATURES");
		System.out.print("\t");
		for (int i = 0; i < features.length; i++) {
			System.out.print(features[i] + "\t");
		}
		System.out.println();
		for (int i = 0; i < segments.size(); i++) {
			System.out.print(segments.get(i) + "\t");
			for (int j = 0; j < segmentFeatures.get(segments.get(i)).length; j++) {
				System.out.print(segmentFeatures.get(segments.get(i))[j] + "\t");
			}
			System.out.println();
		}

		// Find the longest common prefix for each row
		ArrayList<String> commonPrefix = new ArrayList<String>();
		for (int i = 0; i < paradigm.size(); i++) {
			String lcp = paradigm.get(i)[0];
			for (int j = 1; j < paradigm.get(i).length; j++) {
				String str = paradigm.get(i)[j];
				// Which string is shorter
				int shorter = lcp.length();
				if (str.length() < shorter) {
					shorter = str.length();
				}
				// Find leftmost mismatch; update lcp
				for (int k = 0; k < shorter; k++) {
					if (!lcp.substring(k, k+1).equals(str.substring(k, k+1))) {
						lcp = lcp.substring(0, k);
						if (k >= lcp.length()) {
							break;
						}
					}
				}
			}
			commonPrefix.add(lcp);
//			System.out.println(lcp);
		}

		// Find the longest common suffix for each column
		ArrayList<String> commonSuffix = new ArrayList<String>();
		for (int i = 0; i < paradigm.get(0).length; i++) {
			String lcs = new StringBuffer(paradigm.get(0)[i]).reverse().toString();
			for (int j = 1; j < paradigm.size(); j++) {
				String str = new StringBuffer(paradigm.get(j)[i]).reverse().toString();
				// Which string is shorter
				int shorter = lcs.length();
				if (str.length() < shorter) {
					shorter = str.length();
				}
				// Find leftmost mismatch; update lcs
				for (int k = 0; k < shorter; k++) {
					if (!lcs.substring(k, k+1).equals(str.substring(k, k+1))) {
						lcs = lcs.substring(0, k);
						if (k >= lcs.length()) {
							break;
						}
					}
				}
			}
			lcs = new StringBuffer(lcs).reverse().toString();
			// Greedy stem assignment: make sure shared material belongs to LCP
			System.out.println(lcs);
			if (lcs.length() == paradigm.get(0)[0].length()) {
				int lenLCP = commonPrefix.get(i).length();
				for (int j = 0; j < lenLCP; j++) {
					lcs = lcs.substring(1);
				}
			}
			commonSuffix.add(lcs);
//			System.out.println(lcs);
		}

		System.out.println();
		// Parse paradigm by longest common prefixes and suffixes
		ArrayList<String[]> parsedParadigm = new ArrayList<String[]>();
		for (int i = 0; i < paradigm.size(); i++) {
			String[] parsedRow = new String[paradigm.get(i).length];
			String rowLCP = commonPrefix.get(i);
			for (int j = 0; j < paradigm.get(i).length; j++) {
				String colLCS = commonSuffix.get(j);
				int lcpIndex = rowLCP.length();
				int lcsIndex = paradigm.get(i)[j].lastIndexOf(colLCS);
				if (lcsIndex > lcpIndex) {
					parsedRow[j] = rowLCP + "-" + paradigm.get(i)[j].substring(lcpIndex, lcsIndex) + "-" + colLCS;
				} else {
					parsedRow[j] = rowLCP + "--" + colLCS;
				}
				System.out.print(parsedRow[j] + "\t");
			}
			parsedParadigm.add(parsedRow);
			System.out.println();
		}

		// Option 0: Set all features to ? [50% for all features]
		// Option 1: Identity and ?
		// Option 2: Identity and seen probability

		// Divide into morphology
		int option = 2;

		ArrayList<ArrayList<double[]>> stemFeatures = new ArrayList<ArrayList<double[]>>();
		ArrayList<ArrayList<double[]>> suffixFeatures = new ArrayList<ArrayList<double[]>>();

		if (option == 0) {
			// Set all features to 0.5
			// Set stems
			for (int i = 0; i < parsedParadigm.size(); i++) {
				// New stem for each row
				ArrayList<double []> tempStem = new ArrayList<double []>();
				double [] tempStemSeg = new double [features.length + 1];
				// Process LCP for row
				if (commonPrefix.get(i) != "") {
					int rowLCPLen = commonPrefix.get(i).length();
					for (int j = 0; j < rowLCPLen; j++) {
						tempStemSeg = new double[features.length + 1];
						Arrays.fill(tempStemSeg, 0.5);
						tempStem.add(tempStemSeg);
					}
				}
				// Find longest unknown for row
				int longestUnknown = -1;
				for (int j = 0; j < parsedParadigm.get(i).length; j++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				// Process longest unknown
				for (int j = 0; j < longestUnknown; j++) {
					tempStemSeg = new double[features.length + 1];
					Arrays.fill(tempStemSeg, 0.5);
					tempStem.add(tempStemSeg);
				}
				stemFeatures.add(tempStem);
			}
			// Set suffixes
			for (int j = 0; j < parsedParadigm.get(0).length; j++) {
				// New suffix for each column
				ArrayList<double []> tempSuffix = new ArrayList<double []>();
				double [] tempSuffixSeg = new double [features.length + 1];
				// Find longest unknown for col
				int longestUnknown = -1;
				for (int i = 0; i < parsedParadigm.size(); i++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				// Process longest unknown
				for (int k = 0; k < longestUnknown; k++) {
					tempSuffixSeg = new double[features.length + 1];
					Arrays.fill(tempSuffixSeg, 0.5);
					tempSuffix.add(tempSuffixSeg);
				}
				// Process LCS for column
				if (commonSuffix.get(j) != "") {
					int colLCSLen = commonSuffix.get(j).length();
					for (int k = 0; k < colLCSLen; k++) {
						tempSuffixSeg = new double[features.length + 1];
						Arrays.fill(tempSuffixSeg, 0.5);
						tempSuffix.add(tempSuffixSeg);
					}
				}
				suffixFeatures.add(tempSuffix);
			}
		} else if (option == 1) {
			// Keep identity, otherwise set all features to 0.5
			// Set stems
			for (int i = 0; i < parsedParadigm.size(); i++) {
				// New stem for each row
				ArrayList<double []> tempStem = new ArrayList<double []>();
				double [] tempStemSeg = new double [features.length + 1];
				// Process LCP for row
				if (commonPrefix.get(i) != "") {
					int rowLCPLen = commonPrefix.get(i).length();
					// Iterate over row LCP segments
					for (int j = 0; j < rowLCPLen; j++) {
						tempStemSeg = new double[features.length + 1];
						tempStemSeg[0] = 1.0;
						double [] LCPSegFeats = segmentFeatures.get(commonPrefix.get(i).substring(j, j+1));
						for (int k = 0; k < features.length; k++) {
							tempStemSeg[k+1] = LCPSegFeats[k];
						}
						tempStem.add(tempStemSeg);
					}
				}
				// Process unknowns; left-aligned
				// Find longest unknown for row
				int longestUnknown = -1;
				for (int j = 0; j < parsedParadigm.get(i).length; j++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				// Process longest unknown
				for (int j = 0; j < longestUnknown; j++) {
					tempStemSeg = new double[features.length + 1];
					Arrays.fill(tempStemSeg, 0.5);
					tempStem.add(tempStemSeg);
				}
				stemFeatures.add(tempStem);
			}
			// Set suffixes
			for (int j = 0; j < parsedParadigm.get(0).length; j++) {
				// New suffix for each column
				ArrayList<double []> tempSuffix = new ArrayList<double []>();
				double [] tempSuffixSeg = new double [features.length + 1];
				// Find longest unknown for col
				int longestUnknown = -1;
				for (int i = 0; i < parsedParadigm.size(); i++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				// Process longest unknown
				for (int k = 0; k < longestUnknown; k++) {
					tempSuffixSeg = new double[features.length + 1];
					Arrays.fill(tempSuffixSeg, 0.5);
					tempSuffix.add(tempSuffixSeg);
				}
				// Process LCS for column
				if (commonSuffix.get(j) != "") {
					int colLCSLen = commonSuffix.get(j).length();
					// Iterate over col LCS segments
					for (int k = 0; k < colLCSLen; k++) {
						tempSuffixSeg = new double[features.length + 1];
						tempSuffixSeg[0] = 1.0;
						double [] LCSSegFeats = segmentFeatures.get(commonSuffix.get(j).substring(k, k+1));
						for (int l = 0; l < features.length; l++) {
							tempSuffixSeg[l+1] = LCSSegFeats[l];
						}
						tempSuffix.add(tempSuffixSeg);
					}
				}
				suffixFeatures.add(tempSuffix);
			}
		} else if (option == 2) {
			// Keep identity; otherwise set features to observed values
			// Set stems
			for (int i = 0; i < parsedParadigm.size(); i++) {
				// New stem for each row
				ArrayList<double []> tempStem = new ArrayList<double []>();
				double [] tempStemSeg = new double [features.length + 1];
				// Process LCP for row
				if (commonPrefix.get(i) != "") {
					int rowLCPLen = commonPrefix.get(i).length();
					// Iterate over row LCP segments
					for (int j = 0; j < rowLCPLen; j++) {
						tempStemSeg = new double[features.length + 1];
						tempStemSeg[0] = 1.0;
						double [] LCPSegFeats = segmentFeatures.get(commonPrefix.get(i).substring(j, j+1));
						for (int k = 0; k < features.length; k++) {
							tempStemSeg[k+1] = LCPSegFeats[k];
						}
						tempStem.add(tempStemSeg);
					}
				}
				// Process unknowns; left-aligned
				// Find longest unknown for row
				int longestUnknown = -1;
				for (int j = 0; j < parsedParadigm.get(i).length; j++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				double [][] tempUnknownStem = new double [longestUnknown][features.length + 1];
				for (int j = 0; j < tempUnknownStem.length; j++) {
					Arrays.fill(tempUnknownStem[j], 0.0);
				}

				// Iterate over unknowns
				for (int j = 0; j < parsedParadigm.get(i).length; j++) {
					String cellUnknown = parsedParadigm.get(i)[j].split("-")[1];
					// Iterate over segments
					for (int k = 0; k < cellUnknown.length(); k++) {
						double [] unkSegFeats = segmentFeatures.get(cellUnknown.substring(k, k+1));
						tempUnknownStem[k][0] += 1;
						for (int l = 0; l < features.length; l++) {
							tempUnknownStem[k][l + 1] += unkSegFeats[l];
						}
					}
				}
				// Divide seen features by number of times seen
				for (int j = 0; j < tempUnknownStem.length; j++) {
					double numberSeen = tempUnknownStem[j][0];
					// Divide existence counts by number of unknowns
					tempUnknownStem[j][0] = tempUnknownStem[j][0] / parsedParadigm.get(i).length;
					// Divide feature counts by number of times segment was seen
					for (int k = 0; k < features.length; k++) {
						tempUnknownStem[j][k+1] = tempUnknownStem[j][k+1] / numberSeen;
					}
				}
				// Add tempUnknownStem segments into tempStem
				for (int j = 0; j < tempUnknownStem.length; j++) {
					tempStem.add(tempUnknownStem[j]);
				}
				stemFeatures.add(tempStem);
			}
			// Set suffixes
			for (int j = 0; j < parsedParadigm.get(0).length; j++) {
				// New suffix for each column
				ArrayList<double []> tempSuffix = new ArrayList<double []>();
				double [] tempSuffixSeg = new double [features.length + 1];

				// Process unknowns; right-aligned
				// Find longest unknown for col
				int longestUnknown = -1;
				for (int i = 0; i < parsedParadigm.size(); i++) {
					int cellLen = parsedParadigm.get(i)[j].split("-")[1].length();
					if (cellLen > longestUnknown) {
						longestUnknown = cellLen;
					}
				}
				double [][] tempUnknownSuffix = new double [longestUnknown][features.length + 1];
				for (int k = 0; k < tempUnknownSuffix.length; k++) {
					Arrays.fill(tempUnknownSuffix[k], 0.0);
				}

				// Iterate over unknowns
				for (int i = 0; i < parsedParadigm.size(); i++) {
					String cellUnknown = parsedParadigm.get(i)[j].split("-")[1];
					// Iterate over segments right-to-left
					for (int k = cellUnknown.length() - 1; k > -1; k--) {
						double [] unkSegFeats = segmentFeatures.get(cellUnknown.substring(k, k+1));
						tempUnknownSuffix[k + longestUnknown - cellUnknown.length()][0] += 1;
						for (int l = 0; l < features.length; l++) {
							tempUnknownSuffix[k + longestUnknown - cellUnknown.length()][l + 1] += unkSegFeats[l];
						}
					}
				}
				// Divide seen features by number of times seen
				for (int k = 0; k < tempUnknownSuffix.length; k++) {
					double numberSeen = tempUnknownSuffix[k][0];
					// Divide existence counts by number of unknowns
					tempUnknownSuffix[k][0] = tempUnknownSuffix[k][0] / parsedParadigm.size();
					// Divide feature counts by number of times segment was seen
					for (int l = 0; l < features.length; l++) {
						tempUnknownSuffix[k][l+1] = tempUnknownSuffix[k][l+1] / numberSeen;
					}
				}
				// Add tempUnknownSuffix segments into tempSuffix
				for (int k = 0; k < tempUnknownSuffix.length; k++) {
					tempSuffix.add(tempUnknownSuffix[k]);
				}

				// Process LCS for column
				if (commonSuffix.get(j) != "") {
					int colLCSLen = commonSuffix.get(j).length();
					// Iterate over col LCS segments
					for (int k = 0; k < colLCSLen; k++) {
						tempSuffixSeg = new double[features.length + 1];
						tempSuffixSeg[0] = 1.0;
						double [] LCSSegFeats = segmentFeatures.get(commonSuffix.get(j).substring(k, k+1));
						for (int l = 0; l < features.length; l++) {
							tempSuffixSeg[l+1] = LCSSegFeats[l];
						}
						tempSuffix.add(tempSuffixSeg);
					}
				}
				suffixFeatures.add(tempSuffix);
			}
		}

		System.out.println();
		System.out.println("Option " + option);
		System.out.println("\nStems");
		for (int i = 0; i < stemFeatures.size(); i++) {
			for (int j = 0; j < stemFeatures.get(i).size(); j++) {
				for (int k = 0; k < stemFeatures.get(i).get(j).length; k++) {
					System.out.printf("%.2f", stemFeatures.get(i).get(j)[k]);
					System.out.print(" ");
				}
				System.out.println();
			}
			for (int j = 0; j < stemFeatures.get(i).size(); j++) {
				System.out.print(generateSegment(stemFeatures.get(i).get(j), featuresSegment));
			}
			System.out.println();
		}
		System.out.println("\nSuffixes");
		for (int i = 0; i < suffixFeatures.size(); i++) {
			for (int j = 0; j < suffixFeatures.get(i).size(); j++) {
				for (int k = 0; k < suffixFeatures.get(i).get(j).length; k++) {
					System.out.printf("%.2f", suffixFeatures.get(i).get(j)[k]);
					System.out.print(" ");
				}
				System.out.println();
			}
			for (int j = 0; j < suffixFeatures.get(i).size(); j++) {
				System.out.print(generateSegment(suffixFeatures.get(i).get(j), featuresSegment));
			}
			System.out.println();
		}
	}

	// Method to randomly generated a segment from a feature list
	public static String generateSegment(double [] features, Map<String, String> featuresSegment) {
		String randomFeatures = "";
		String result = "";
		double randomDouble = Math.random();
		if (randomDouble < features[0]) {
			do {
				randomFeatures = "";
				for (int i = 1; i < features.length; i++) {
					randomDouble = Math.random();
					if (randomDouble < features[i]) {
						randomFeatures = randomFeatures + "1.0,";
					} else {
						randomFeatures = randomFeatures + "0.0,";
					}
				}
				if (featuresSegment.containsKey(randomFeatures)) {
					result = featuresSegment.get(randomFeatures);
				} else {
					result = "?";
				}
//				System.out.println(result + " " + randomFeatures);
			} while (result.equals("?"));
		} else {
			result = "";
		}
		return result;
	}

}
